
import React from "react";
import Header from '../header/header';
import './home.css';
import moment from 'moment';
import 'moment/locale/es';
class Home extends React.Component {
  state = {
    hasErrors: false,
    weather: {},
    city: {},
    list: [],
    days: [],
    date: ''
  };

  componentDidMount() {
    fetch("http://api.openweathermap.org/data/2.5/forecast?id=3873544&appid=093c63d1d6dd2f0f77c6f14d91a19042&units=metric")
      .then(res => res.json())
      .then((res) => {
        this.setState({
          weather: res,
          city: res.city,
          list: res.list
        });
        this.getDay(this.props.match.params.day);
      })
      .catch(() => this.setState({ hasErrors: true }));
  }
  getDay(day) {
    this.state.list.map((item) => {
      const dayName = moment.unix(item.dt).format('dddd');
      if (dayName === day) {
        this.state.days.push(item);
        this.setState({ days: this.state.days, date: moment.unix(item.dt).format('LLLL') });
      }
    });

  }
  render() {

    return (
      <div>
        <Header />
        <h2 className="title-page">{this.state.city.name}, {this.state.date}</h2>
        <div className="container-fluid">
          <div className="row">
            {
              this.state.days.map((item) => {
                console.log(item);
                moment.locale('es');
                return (
                  <div className="col-12 col-sm-4 col-md-2 mt-2">
                    <div className="card" id={item.dt}>
                      <img src={`https://openweathermap.org/img/w/${item.weather[0].icon}.png`} alt="" className="icon-weather" />
                      <div className="card-body">
                        <h5 className="card-title"><span>{moment.unix(item.dt).format('h:mm:ss a')}</span></h5>
                        <h6 className="card-subtitle mb-2 text-muted"><span>Temp.: {item.main.temp}</span></h6>
                      </div>
                    </div>
                  </div>
                )
              })
            }

          </div>
        </div>
      </div>
    );
  }
}
export default Home;