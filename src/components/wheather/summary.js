import React from "react";
import './summary.css';
import moment from 'moment';
import 'moment/locale/es';

class Summary extends React.Component {
    xdomain = []
    ydomain = []
    grapWidth = 300
    grapHeight = 500

    render() {
        return (
            <div>
                <h1 className="title-page">{this.props.city.name}</h1>
                <div className="container-fluid">
                    <div className="row">
                        {
                            this.props.list.map((item) => {
                                console.log(item);
                                moment.locale('es');
                                return (
                                    <div className="col-12 col-sm-4 col-md-2 mt-2">
                                        <div className="card">
                                            <img src={`https://openweathermap.org/img/w/${item.weather[0].icon}.png`} alt="" className="icon-weather" />
                                            <div className="card-body">
                                                <h5 className="card-title"><span>{moment.unix(item.dt).format('ddd, MMM Do')}</span></h5>
                                                <h6 className="card-subtitle mb-2 text-muted"><span>Maxima: {item.temp.max}</span></h6>
                                                <h6 className="card-subtitle mb-2 text-muted"><span>Mínima: {item.temp.min}</span></h6>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }

                    </div>
                </div>
            </div>
        );
    }
}
export default Summary;