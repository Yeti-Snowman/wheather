import React from "react";
import './header.css';
import { getCurrentUser } from '../../services/userService';

class Header extends React.Component {
    user = getCurrentUser();
    render() {
        return (
            <header className="header">
                <div className="user-info">
                    <img src={this.user.avatar} className = "user-avatar" alt="" />
                </div>
            </header>
        );
    }
}
export default Header;