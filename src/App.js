import React, { Component } from "react";
import Summary from './components/wheather/summary';
import Header from "./components/header/header";

export default class App extends Component {
  state = {
    hasErrors: false,
    weather: {},
    city: {},
    list: []
  };

  componentDidMount() {
    fetch("http://api.openweathermap.org/data/2.5/forecast/daily?id=3873544&APPID=093c63d1d6dd2f0f77c6f14d91a19042&cnt=5&units=metric")
      .then(res => res.json())
      .then((res) => {
        this.setState({
          weather: res,
          city: res.city,
          list: res.list
        });
      })
      .catch(() => this.setState({ hasErrors: true }));
  }

  render() {
    if (this.state.city) {
      return <div>
        <Header />
        <Summary city={this.state.city} list={this.state.list} />
      </div>
    } else {
      return <div>
        <h1 className="title-page">Loading</h1>
      </div>
    }

  }
}
